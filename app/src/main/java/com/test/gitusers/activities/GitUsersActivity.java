package com.test.gitusers.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.squareup.otto.Subscribe;
import com.test.gitusers.GithubApp;
import com.test.gitusers.R;
import com.test.gitusers.adapters.GitPages;
import com.test.gitusers.controls.DisableableViewPager;
import com.test.gitusers.events.FinishedGettingUsers;
import com.test.gitusers.events.SelectedUser;
import com.test.gitusers.events.StartedGettingUsers;

public class GitUsersActivity extends AppCompatActivity {

    private DisableableViewPager mViewPager;
    private GitPages mPagerAdapter;

    private TabLayout mTabLayout;

    private ProgressBar mProgress;

    private GithubApp mApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_git_users);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mApp = (GithubApp) getApplication();

        mPagerAdapter = new GitPages(mApp.getEventBus());
        mViewPager = (DisableableViewPager) findViewById(R.id.viewpager);
        mViewPager.setAdapter(mPagerAdapter);

        mTabLayout = (TabLayout) findViewById(R.id.tablayout);
        mTabLayout.setupWithViewPager(mViewPager);

        mProgress = (ProgressBar) findViewById(R.id.progress);

        setupProgressBar();

        handleIntent(getIntent());
    }

    private void setupProgressBar() {
        Drawable drawableProgress = mProgress.getIndeterminateDrawable().mutate();
        Drawable drawable = mProgress.getProgressDrawable();

        int height = Math.max(drawable.getMinimumHeight(), drawableProgress.getMinimumHeight());
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) mProgress.getLayoutParams();
        params.setMargins(0, -height / 2, 0, 0);
        mProgress.setLayoutParams(params);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mApp.getEventBus().register(this);
        mApp.getmGithubApi().updateUsers();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mApp.getEventBus().unregister(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final MenuItem searchMenu =
                menu.findItem(R.id.search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchMenu);
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setIconifiedByDefault(false);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filterList(newText);
                return false;
            }
        });
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    startFiltration();
                } else {
                    MenuItemCompat.collapseActionView(searchMenu);
                    stopFiltration();
                }
            }
        });

        return true;
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            filterList(query);
        }
    }

    private void filterList(String filter) {
        mPagerAdapter.overrideFiltration(filter);
    }

    private void startFiltration() {
        mTabLayout.setVisibility(View.GONE);
        mViewPager.setEnabled(false);
        filterList("");
    }

    private void stopFiltration() {
        mPagerAdapter.resetFiltration();
        mTabLayout.setVisibility(View.VISIBLE);
        mViewPager.setEnabled(true);
    }

    @Subscribe
    public void onStartUsersLoading(StartedGettingUsers start) {
        mProgress.setVisibility(View.VISIBLE);
    }

    @Subscribe
    public void onFinishUsersLoading(FinishedGettingUsers finish) {
        mProgress.setVisibility(View.GONE);
    }

    @Subscribe
    public void onSelectedUser(SelectedUser user) {
        Toast.makeText(this,
                "User selected: " + user.user.nick + " id: " + user.user.id, Toast.LENGTH_SHORT)
                .show();
    }
}
