package com.test.gitusers.model;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by Vitaly on 25.12.2015.
 */
@Database(name = GithubUsersDb.NAME, version = GithubUsersDb.VERSION)
public class GithubUsersDb {
    public static final String NAME = "Github";
    public static final int VERSION = 1;
}
