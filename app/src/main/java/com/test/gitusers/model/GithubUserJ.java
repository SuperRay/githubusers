package com.test.gitusers.model;

/**
 * Created by Vitaly on 25.12.2015.
 */
public class GithubUserJ {
    public String avatar_url;
    public String events_url;
    public String followers_url;
    public String following_url;
    public String gists_url;
    public String gravatar_id;
    public String html_url;
    public long id;
    public String login;
    public String organizations_url;
    public String received_events_url;
    public String repos_url;
    public boolean site_admin;
    public String starred_url;
    public String subscriptions_url;
    public String type;
    public String url;

}
