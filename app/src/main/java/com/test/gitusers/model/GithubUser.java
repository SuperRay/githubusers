package com.test.gitusers.model;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by Vitaly on 25.12.2015.
 */
@Table(database = GithubUsersDb.class)
public class GithubUser extends BaseModel {
    @PrimaryKey(autoincrement = false)
    public long id;

    @Column
    public String nick;

    @Column
    public String photoUrl;

    @Column
    public String followersUrl;

    @Column
    public String followingUrl;

    @Column
    public int followersCount;

    @Column
    public int followingCount;
}
