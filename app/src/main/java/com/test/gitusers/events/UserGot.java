package com.test.gitusers.events;

import com.test.gitusers.model.GithubUser;

/**
 * Created by Vitaly on 26.12.2015.
 */
public class UserGot {
    public GithubUser user;

    public UserGot(GithubUser user) {
        this.user = user;
    }
}
