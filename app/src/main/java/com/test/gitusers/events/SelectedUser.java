package com.test.gitusers.events;

import com.test.gitusers.model.GithubUser;

/**
 * Created by Vitaly on 26.12.2015.
 */
public class SelectedUser {
    public GithubUser user;

    public SelectedUser(GithubUser user) {
        this.user = user;
    }
}
