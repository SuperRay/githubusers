package com.test.gitusers.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.raizlabs.android.dbflow.annotation.Collate;
import com.raizlabs.android.dbflow.sql.language.From;
import com.raizlabs.android.dbflow.sql.language.OrderBy;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;
import com.test.gitusers.R;
import com.test.gitusers.events.SelectedUser;
import com.test.gitusers.events.UserGot;
import com.test.gitusers.model.GithubUser;
import com.test.gitusers.model.GithubUser_Table;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vitaly on 24.12.2015.
 */
public class GithubUsersAdapter extends RecyclerView.Adapter<GithubUsersAdapter.GithubUserViewHolder> {

    private final static int FILTER_NONE = 0;
    private final static int FILTER_TEXT = 1;
    private final static int FILTER_RANGE = 2;

    private Context mContext;

    private int mFilterType;
    private String mFilter;

    private List<GithubUser> mUsers = new ArrayList<>();

    private Bus mBus;

    public GithubUsersAdapter(Context context, Bus bus) {
        mContext = context;
        mBus = bus;
        mBus.register(this);
        updateList();
    }

    @Override
    public GithubUserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View userView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_github_user, parent, false);
        return new GithubUserViewHolder(userView, mBus);
    }

    @Override
    public void onBindViewHolder(GithubUserViewHolder holder, int position) {
        GithubUser user = mUsers.get(position);
        holder.mHolderView.setTag(user);
        if (!TextUtils.isEmpty(user.photoUrl)) {
            Picasso
                    .with(mContext)
                    .load(Uri.parse(user.photoUrl))
                    .placeholder(R.drawable.icon_github)
                    .error(R.drawable.icon_github)
                    .into(holder.mPhoto);
        }
        holder.mNickName.setText(user.nick);
        holder.mStats.setText(String.format("%d/%d", user.followersCount, user.followingCount));
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    public void setFilter(String filter) {
        if (TextUtils.isEmpty(filter)) {
            clearFilter();
            return;
        }

        mFilterType = FILTER_TEXT;
        mFilter = "%" + filter + "%";
        updateList();
    }

    public void setFilter(char start, char end) {
        mFilterType = FILTER_RANGE;
        mFilter = String.format("[%c-%c%C-%C]*", start, end, start, end);
        updateList();
    }

    public void setFilterInverse(char start, char end) {
        mFilterType = FILTER_RANGE;
        mFilter = String.format("[^%c-%c%C-%C]*", start, end, start, end);
        updateList();
    }

    public void clearFilter() {
        mFilterType = FILTER_NONE;
        mFilter = null;
        updateList();
    }

    private void updateList() {
        From<GithubUser> selection = SQLite.select()
                .from(GithubUser.class);
        switch (mFilterType) {
            case FILTER_TEXT:
                mUsers = selection
                        .where(GithubUser_Table.nick.like(mFilter))
                        .orderBy(OrderBy.fromProperty(GithubUser_Table.nick)
                                .ascending()
                                .collate(Collate.NOCASE))
                        .queryList();
                break;
            case FILTER_RANGE:
                mUsers = selection
                        .where(GithubUser_Table.nick.glob(mFilter))
                        .orderBy(OrderBy.fromProperty(GithubUser_Table.nick)
                                .ascending()
                                .collate(Collate.NOCASE))
                        .queryList();
                break;
            default:
                mUsers = selection
                        .orderBy(OrderBy.fromProperty(GithubUser_Table.nick)
                                .ascending()
                                .collate(Collate.NOCASE))
                        .queryList();
        }

        notifyDataSetChanged();
    }

    @Subscribe
    public void onUserGot(UserGot user) {
        updateList();
    }

    public static class GithubUserViewHolder extends RecyclerView.ViewHolder {

        public View mHolderView;

        public ImageView mPhoto;

        public TextView mNickName;

        public TextView mStats;

        public GithubUserViewHolder(final View itemView, final Bus mBus) {
            super(itemView);
            mHolderView = itemView;
            mPhoto = (ImageView) itemView.findViewById(R.id.photo);
            mNickName = (TextView) itemView.findViewById(R.id.nickname);
            mStats = (TextView) itemView.findViewById(R.id.stats);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mBus.post(new SelectedUser((GithubUser) itemView.getTag()));
                }
            });
        }
    }
}
