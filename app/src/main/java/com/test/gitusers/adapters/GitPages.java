package com.test.gitusers.adapters;

import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.otto.Bus;
import com.test.gitusers.R;

import java.util.HashMap;

/**
 * Created by Vitaly on 24.12.2015.
 */
public class GitPages extends PagerAdapter {
    private HashMap<Integer, RecyclerView> mTabs = new HashMap<>();
    private RecyclerView mPrimaryView;
    private int mPrimaryPosition;

    private Bus mBus;

    public GitPages(Bus eventBus) {
        mBus = eventBus;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        RecyclerView list = mTabs.get(position);
        if (list == null) {
            list = (RecyclerView) LayoutInflater.from(container.getContext()).inflate(
                    R.layout.content_git_users, container, false);
            list.setLayoutManager(new LinearLayoutManager(container.getContext()));
            GithubUsersAdapter adapter = new GithubUsersAdapter(container.getContext(), mBus);
            setFilter(position, adapter);
            list.setAdapter(adapter);
            mTabs.put(position, list);
        }
        container.addView(list);
        return list;
    }

    private void setFilter(int position, GithubUsersAdapter adapter) {
        switch (position) {
            case 0:
                adapter.setFilter('a', 'h');
                break;
            case 1:
                adapter.setFilter('i', 'p');
                break;
            case 2:
                adapter.setFilter('q', 'z');
                break;
        }
    }

    private void setFilter(String filter, GithubUsersAdapter adapter) {
        adapter.setFilter(filter);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
        mPrimaryView = mTabs.get(position);
        mPrimaryPosition = position;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "A-H";
            case 1:
                return "I-P";
            case 2:
                return "Q-Z";
            default:
                return null;
        }
    }

    public void overrideFiltration(String filter) {
        setFilter(filter,
                (GithubUsersAdapter) mPrimaryView.getAdapter());
    }

    public void resetFiltration() {
        setFilter(mPrimaryPosition,
                (GithubUsersAdapter) mPrimaryView.getAdapter());
    }
}
