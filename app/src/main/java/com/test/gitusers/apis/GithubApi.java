package com.test.gitusers.apis;

import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.otto.Bus;
import com.test.gitusers.events.FinishedGettingUsers;
import com.test.gitusers.events.StartedGettingUsers;
import com.test.gitusers.events.UserGot;
import com.test.gitusers.model.GithubUser;
import com.test.gitusers.model.GithubUserJ;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Vitaly on 26.12.2015.
 */
public class GithubApi {
    private final static String GITHUB_USERS = "https://api.github.com/users";

    private final static String GITHUB_PREFS = "github_data";
    private final static String GITHUB_USER_LAST = "github_user_last";
    private final static String GITHUB_RESET_TIME = "github_reset_time";

    public static final String PER_PAGE_VALUE = "100";

    public static final String PER_PAGE_PARAM = "per_page";
    public static final String SINCE_PARAM = "since";

    public static final String PAGE_LAST = "last";
    public static final String PAGE_NEXT = "next";

    public static final String RATE_LIMIT_HEADER = "X-RateLimit-Remaining";
    public static final String RESET_LIMIT_HEADER = "X-RateLimit-Reset";
    public static final String LINK_HEADER = "Link";

    Handler mHandler = new Handler(Looper.getMainLooper());

    private boolean mUpdating;

    private OkHttpClient mClient;
    private Bus mEventBus;
    private SharedPreferences mPrefs;

    private Gson gson = new Gson();

    private Pattern mLinkPattern = Pattern.compile("<(.*?)> *?; *?rel=\"(.*?)\"", Pattern.CASE_INSENSITIVE);
    private Pattern mPagePattern = Pattern.compile("[\\?&]page=([0-9]*?)(&.*)?$", Pattern.CASE_INSENSITIVE);
    private Pattern mOptionalPattern = Pattern.compile("\\{.*?\\}", Pattern.CASE_INSENSITIVE);

    public GithubApi(Context context, OkHttpClient networkClient, Bus eventBus) {
        mClient = networkClient;
        mEventBus = eventBus;
        mPrefs = context.getSharedPreferences(GITHUB_PREFS, Context.MODE_PRIVATE);
    }

    public void updateUsers() {
        mEventBus.post(new StartedGettingUsers());

        long reset = mPrefs.getLong(GITHUB_RESET_TIME, 0);
        if (reset > 0 && new Date().getTime() / 1000 < reset) {
            mEventBus.post(new FinishedGettingUsers());
            return;
        }

        if (mUpdating) {
            mEventBus.post(new FinishedGettingUsers());
            return;
        }
        mUpdating = true;

        Long lastUserId = mPrefs.getLong(GITHUB_USER_LAST, 0);
        HttpUrl.Builder urlBuilder = HttpUrl.parse(GITHUB_USERS).newBuilder()
                .addQueryParameter(PER_PAGE_PARAM, PER_PAGE_VALUE);
        if (lastUserId != 0) {
            urlBuilder.addQueryParameter(SINCE_PARAM, lastUserId.toString());
        }

        new UsersAsyncTask().execute(urlBuilder.build().toString());
    }

    private String updateUsers(String usersUrl) {
        Request request = new Request.Builder()
                .url(usersUrl)
                .build();

        String next = null;
        try {
            Response response = mClient.newCall(request).execute();

            try {
                saveResetTime(Long.parseLong(response.header(RATE_LIMIT_HEADER)),
                        Long.parseLong(response.header(RESET_LIMIT_HEADER)));
            } catch (NumberFormatException ignored) {

            }

            if (!response.isSuccessful()) {
                return null;
            }

            List<GithubUserJ> users = gson
                    .fromJson(response.body().charStream(), new TypeToken<List<GithubUserJ>>() {
                    }.getType());

            for (GithubUserJ user : users) {
                final GithubUser gu = getUser(user);
                gu.save();
                mPrefs.edit().putLong(GITHUB_USER_LAST, gu.id).apply();
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mEventBus.post(new UserGot(gu));
                    }
                });
            }

            next = getLinkUrl(response.header(LINK_HEADER), PAGE_NEXT);
        } catch (IOException | NetworkErrorException e) {
            e.printStackTrace();
        }

        return next;
    }

    private void saveResetTime(long remaining, long reset) {
        if (remaining > 0 || reset == 0) {
            return;
        }

        mPrefs.edit().putLong(GITHUB_RESET_TIME, reset).apply();
    }

    private GithubUser getUser(GithubUserJ user) throws NetworkErrorException {
        GithubUser gu = new GithubUser();
        gu.id = user.id;
        gu.nick = user.login;
        gu.photoUrl = user.avatar_url;
        gu.followersUrl = user.followers_url;
        gu.followersCount = getUsersCount(removeOptionalUrl(user.followers_url));
        gu.followingUrl = user.following_url;
        gu.followingCount = getUsersCount(removeOptionalUrl(user.following_url));

        return gu;
    }

    private int getUsersCount(String url) throws NetworkErrorException {
        return getUsersCount(url, false);
    }

    private int getUsersCount(String url, boolean last) throws NetworkErrorException {
        HttpUrl urlParam = HttpUrl.parse(url).newBuilder()
                .addQueryParameter(PER_PAGE_PARAM, PER_PAGE_VALUE)
                .build();

        Request request = new Request.Builder()
                .url(urlParam)
                .build();

        try {
            Response response = mClient.newCall(request).execute();

            long remain = 0;
            long reset = 0;
            try {
                remain = Long.parseLong(response.header(RATE_LIMIT_HEADER));
                reset = Long.parseLong(response.header(RESET_LIMIT_HEADER));
                saveResetTime(remain, reset);
            } catch (NumberFormatException ignored) {

            }

            if (!response.isSuccessful()) {
                if (remain == 0 && reset != 0)
                    throw new NetworkErrorException("Github rate limit exceeded");

                throw new NetworkErrorException();
            }

            List<GithubUserJ> users = gson
                    .fromJson(response.body().charStream(), new TypeToken<List<GithubUserJ>>() {
                    }.getType());

            if (last) {
                return users.size();
            }

            String lastUrl = getLinkUrl(response.header(LINK_HEADER), PAGE_LAST);
            if (TextUtils.isEmpty(lastUrl)) {
                return users.size();
            }

            Matcher matcher = mPagePattern.matcher(lastUrl);
            if (!matcher.find()) {
                return users.size();
            }

            int pages = 1;
            try {
                pages = Integer.parseInt(matcher.group(1)) - 1;
            } catch (NumberFormatException ignored) {

            }
            int count = getUsersCount(lastUrl, true);

            return users.size() * pages + count;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return 0;
    }

    private String getLinkUrl(String links, String type) {
        if (TextUtils.isEmpty(links) || TextUtils.isEmpty(type))
            return null;

        Matcher matcher = mLinkPattern.matcher(links);
        while (matcher.find()) {
            if (matcher.group(2).equalsIgnoreCase(type)) {
                return matcher.group(1);
            }
        }

        return null;
    }

    private String removeOptionalUrl(String url) {
        if (TextUtils.isEmpty(url))
            return null;

        Matcher matcher = mOptionalPattern.matcher(url);
        return matcher.replaceAll("");
    }

    private class UsersAsyncTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            String next = params[0];
            do {
                try {
                    next = updateUsers(next);
                } catch (Exception e) {
                    e.printStackTrace();
                    next = null;
                }
            } while (!TextUtils.isEmpty(next));

            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            mEventBus.post(new FinishedGettingUsers());
            mUpdating = false;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mEventBus.post(new FinishedGettingUsers());
            mUpdating = false;
        }
    }
}
