package com.test.gitusers;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.otto.Bus;
import com.test.gitusers.apis.GithubApi;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Vitaly on 25.12.2015.
 */
public class GithubApp extends Application {

    private OkHttpClient mNetworkClient = new OkHttpClient();
    private Bus mBus = new Bus();
    private GithubApi mGithubApi;

    @Override
    public void onCreate() {
        super.onCreate();
        final Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .build();
        Fabric.with(fabric);
        FlowManager.init(this);
        mGithubApi = new GithubApi(this, mNetworkClient, mBus);

    }

    public Bus getEventBus() {
        return mBus;
    }

    public GithubApi getmGithubApi() {
        return mGithubApi;
    }
}
